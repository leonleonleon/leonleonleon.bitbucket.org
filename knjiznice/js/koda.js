
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

var moski = ["boy.png","man.png","man-1.png","man-3.png"];
var zenske = ["girl.png", "girl-1.png", "man-4.png"];

var spol = "";

var indeks = 0;

var pacienti = {
  "Dejan":"0333abcd-c56c-4386-9697-16d4e1438d22",
  "Pujsa":"5cdebdfa-0cd7-4d85-a1c1-dad97cee3da3",
  "Ata":"54a6dd28-e357-4054-b363-c272b8b59449"
};

var selectedSymptoms = [];

var bolnikiGeneriranje = [
  {
  ime:"Janez",
  priimek:"Novak",
  spol:"MALE",
  naslov:"Cesta na brdo 32",
  rojstvo: (new Date(1970,12,5,22,4,0,0)).toISOString(),
  podatki:[
    {
    datumInUra: (new Date(2011)).toISOString(),
    telesnaVisina:177,
    telesnaTeza: 66,
    telesnaTemperatura: 32,
    sistolicniKrvniTlak: 123,
    diastolicniKrvniTlak: 78,
    nasicenostKrviSKisikom: 45.2
    },
    {
    datumInUra: (new Date(2011,2,5)).toISOString(),
    telesnaVisina:188,
    telesnaTeza: 87,
    telesnaTemperatura: 32.5,
    sistolicniKrvniTlak: 143,
    diastolicniKrvniTlak: 76,
    nasicenostKrviSKisikom: 46
    },
    {
    datumInUra: (new Date(2013,16,8)).toISOString(),
    telesnaVisina:142,
    telesnaTeza: 98,
    telesnaTemperatura: 35.3,
    sistolicniKrvniTlak: 153,
    diastolicniKrvniTlak: 80,
    nasicenostKrviSKisikom: 47
    },
    {
    datumInUra: (new Date(2020,3,30)).toISOString(),
    telesnaVisina:199,
    telesnaTeza: 126,
    telesnaTemperatura: 38,
    sistolicniKrvniTlak: 163,
    diastolicniKrvniTlak: 73,
    nasicenostKrviSKisikom: 49.9
    }
    ]
  },
  {
  ime:"Branko",
  priimek:"Kovač",
  spol:"MALE",
  naslov:"Dol pri poddupleku 32",
  rojstvo: (new Date(1988,2,23,6,22,0,0)).toISOString(),
  podatki:[
    {
    datumInUra: (new Date(2016, 7,8)).toISOString(),
    telesnaVisina:156,
    telesnaTeza: 45,
    telesnaTemperatura: 36,
    sistolicniKrvniTlak: 170,
    diastolicniKrvniTlak: 56,
    nasicenostKrviSKisikom: 12
    },
    {
    datumInUra: (new Date(2011, 7,12)).toISOString(),
    telesnaVisina:176,
    telesnaTeza: 56,
    telesnaTemperatura: 36,
    sistolicniKrvniTlak: 125,
    diastolicniKrvniTlak: 67,
    nasicenostKrviSKisikom: 55
    },
    {
    datumInUra: (new Date(2005, 12,6)).toISOString(),
    telesnaVisina:186,
    telesnaTeza: 99,
    telesnaTemperatura: 38,
    sistolicniKrvniTlak: 226,
    diastolicniKrvniTlak: 56,
    nasicenostKrviSKisikom: 100
    },
    {
    datumInUra: (new Date(2001, 3,5)).toISOString(),
    telesnaVisina:199,
    telesnaTeza: 47,
    telesnaTemperatura: 36,
    sistolicniKrvniTlak: 333,
    diastolicniKrvniTlak: 23,
    nasicenostKrviSKisikom: 23
    },
    ]
  },
  {
  ime: "Jožefa",
  priimek: "Koritnik",
  spol:"FEMALE",
  naslov:"Nekje 107",
  rojstvo: (new Date(1923,7,15,18,55,0,0)).toISOString(),
  podatki:[
  {
  datumInUra: (new Date(2040,5,6)).toISOString(),
  telesnaVisina: 166,
  telesnaTeza: 98,
  telesnaTemperatura: 66,
  sistolicniKrvniTlak: 265,
  diastolicniKrvniTlak: 55,
  nasicenostKrviSKisikom: 150
  },
  {
  datumInUra: (new Date(2041,12,4)).toISOString(),
  telesnaVisina: 154,
  telesnaTeza: 98,
  telesnaTemperatura: 90,
  sistolicniKrvniTlak: 265,
  diastolicniKrvniTlak: 55,
  nasicenostKrviSKisikom: 120
  },
  {
  datumInUra: (new Date(2050,7,26)).toISOString(),
  telesnaVisina: 112,
  telesnaTeza: 500,
  telesnaTemperatura: 212,
  sistolicniKrvniTlak: 265,
  diastolicniKrvniTlak: 78,
  nasicenostKrviSKisikom: 66.7
  },
  {
  datumInUra: (new Date(2060,5,4)).toISOString(),
  telesnaVisina: 65,
  telesnaTeza: 123,
  telesnaTemperatura: 455,
  sistolicniKrvniTlak: 26,
  diastolicniKrvniTlak: 120,
  nasicenostKrviSKisikom: 23.4
  }
  ]
  },
]
//funkcija za spreminjanje avatarja, ob kliku se avatar zamenja, 7 različnih slik v rotaciji
function changeAvatar(){
  var temp = moski.concat(zenske);
  indeks=(indeks+1)%temp.length;
  $("#avatar").attr("src", "knjiznice/avatar/"+temp[indeks]);
}
//funkcija za dodajanje novega pacienta in dodelitev novega ehrid le-temu
function dodaj(){
  //najprej pridobimo vrednosti vnosnih polj
  var name = $("#inpName").val();
  var surname = $("#inpSurname").val();
  var addressInp = $("#inpAddress").val();
  var dob = $("#inpDOB").val();
  var dobTemp = dob.split(".");
  for(var i = 0; i < dobTemp.length; i++){
    dobTemp[i] = (dobTemp[i].length < 2 ? "0": "")+dobTemp[i];
  }
  //datum spremenimo v pravilen format
  dob = new Date(Date.UTC(parseInt(dobTemp[2]), parseInt(dobTemp[1]), parseInt(dobTemp[0]), 0,0,0,0))
  //ce so vsa polja izpolnjena nadaljujemo, drtugace se pojavi opozorilo
  if(name && surname && addressInp && dob && spol){
    console.log([name, surname, addressInp, dob.toISOString()]);
    //post request, ki kreira nov ehrid in doda osnovne podatke v demographics/party
    $.ajax(baseUrl+"/ehr", {
      type: "POST",
      headers: {
        "Authorization": getAuthorization()
      },
      success: function(data){
        var ehrTemp = data.ehrId;
        console.log(data);
        $.ajax(baseUrl+"/demographics/party",{
          type:"POST",
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: "application/json",
          data: JSON.stringify({
            firstNames: name,
            lastNames: surname,
            gender: spol,
            dateOfBirth: dob.toISOString(),
            address: {
              address: addressInp
            },
            additionalInfo:{
              "ehrId": ehrTemp
            }
          }),
          success: function(party){
            if(party.action == "CREATE"){
              $("#input-ehrId").val(ehrTemp);
              $("#input-div").addClass("has-success");
            }
          }
        });
      }
    });
  }else{
    alert("Prosim izpolnite vsa vnosna polja.")
  }
}
//funkcija ki se poklice ob kliku na gumb "Generiraj podatke" in generira vse 3 paciente v bolnikiGeneriranje
function generirajPodatke(){
  $("#loading").removeClass("hide");
  for(var bolnik of bolnikiGeneriranje){
    generiranje(bolnik);
  }
}
//funkcija katero klice generirajPodatke()
function generiranje(bolnik){
  $.ajax(baseUrl+"/ehr", {
    type: "POST",
    headers: {
      "Authorization": getAuthorization()
    },
    success: function(data){
      var ehrTemp = data.ehrId;
      $.ajax(baseUrl+"/demographics/party",{
        type:"POST",
        headers: {
          "Authorization": getAuthorization()
        },
        contentType: "application/json",
        data: JSON.stringify({
          firstNames: bolnik.ime,
          lastNames: bolnik.priimek,
          gender: bolnik.spol,
          dateOfBirth: bolnik.rojstvo,
          address: {
            address: bolnik.naslov
          },
          additionalInfo:{
            "ehrId": ehrTemp
          }
        }),
        success: function(party){
          if(party.action == "CREATE"){
            var merilec = "Majda Novak";
            for(var i = 0; i < bolnik.podatki.length; i++){
              $.ajax(baseUrl+"/composition?"+$.param({
                ehrId: ehrTemp,
                templateId: "Vital Signs",
                format: "FLAT",
                comitter: merilec
              }),{
                type:"POST",
                contentType:"application/json",
                headers: {
                  "Authorization": getAuthorization()
                },
                data: JSON.stringify({
                  "ctx/time": bolnik.podatki[i].datumInUra,
                  "ctx/language": "en",
                  "ctx/territory": "SI",
                  "vital_signs/body_temperature/any_event/temperature|magnitude": bolnik.podatki[i].telesnaTemperatura,
                  "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                  "vital_signs/blood_pressure/any_event/systolic": bolnik.podatki[i].sistolicniKrvniTlak,
                  "vital_signs/blood_pressure/any_event/diastolic": bolnik.podatki[i].diastolicniKrvniTlak,
                  "vital_signs/height_length/any_event/body_height_length": bolnik.podatki[i].telesnaVisina,
                  "vital_signs/body_weight/any_event/body_weight": bolnik.podatki[i].telesnaTeza,
                  "vital_signs/indirect_oximetry:0/spo2|numerator": bolnik.podatki[i].nasicenostKrviSKisikom
                }),
                success: function(){
                  $("#loading>span").removeClass("glyphicon-refresh spinner-animation");
                  $("#loading>span").addClass("glyphicon-ok");
                  setTimeout(function(){
                    $("#loading").addClass("hide");
                    $("#loading>span").removeClass("glyphicon-ok");
                    $("#loading>span").addClass("glyphicon-refresh spinner-animation");
  
                  },700);
                }
              });
            }
            pacienti[bolnik.ime] = ehrTemp;
            $("#dropdownUsers").append(`<li><a name=${bolnik.ime} href=\"#\">${bolnik.ime} ${bolnik.priimek}</a></li>`);
            updateDropdown();
          }
        }
      });
    }
  });
}
//funkcija za prikaz podatkov izbranega ehrid
function prikaz(){
  renderGraph(document.querySelector("a[getid='height']"));
  var tempPacient = $("#input-ehrId").val();
  $("#diagnoza").attr("disabled", false);
  $("#notedSymptoms").html("");
  $("#symptomSearch").attr("disabled",false);
  $("#firstName, #lastName, #age, #gender, #height, #latestDate, #weight, #temperature, #blood_pressure_s, #blood_pressure_d").html("");
  $("#data").html("");
  selectedSymptoms=[];
  $.ajax(baseUrl+"/demographics/ehr/"+tempPacient+"/party", {
      type: "GET",
      headers: {
          "Authorization": getAuthorization()
      },
      success: function(data){
          $("#firstName").html(data.party.firstNames);
          $("#lastName").html(data.party.lastNames);
          $("#age").html(Math.floor(((new Date()).getTime()-(new Date(data.party.dateOfBirth)).getTime())/(1000*60*60*24*365.25)));
          if(data.party.gender){
            $("#gender").html(data.party.gender=="MALE"?"Moški":"Ženska");
            $("#gender").attr("gender", data.party.gender.toLowerCase());
            $("#avatar").attr("src", "knjiznice/avatar/"+
            (data.party.gender=="MALE"?moski:zenske)[Math.floor(
              Math.random()*(data.party.gender=="MALE"?moski:zenske).length)]);
          }else{
            $("#gender").html("");
            $("#gender").attr("gender", "male");
          }
          $.ajax(baseUrl+"/view/"+tempPacient+"/height",{
            type: "GET",
            headers: {
              "Authorization": getAuthorization()
            },
            success: function(data){
              $("#height").html(data[0].height+data[0].unit);
              $("#height").attr("data", data[0].height);
              $("#latestDate").html((new Date(data[0].time)).toLocaleString());
            },
            error: function(){
              alert("prišlo je do napake");
            }
          });
          $.ajax(baseUrl+"/view/"+tempPacient+"/weight",{
            type: "GET",
            headers: {
              "Authorization": getAuthorization()
            },
            success: function(data){
              $("#weight").html(data[0].weight+data[0].unit);
              $("#weight").attr("data", data[0].weight);
            },
            error: function(){
              alert("prišlo je do napake");
            }
          });
          $.ajax(baseUrl+"/view/"+tempPacient+"/blood_pressure",{
            type: "GET",
            headers: {
              "Authorization": getAuthorization()
            },
            success: function(data){
              $("#blood_pressure_s").html(data[0].systolic+data[0].unit);
              $("#blood_pressure_d").html(data[0].diastolic+data[0].unit);
            },
            error: function(){
              alert("prišlo je do napake");
            }
          });
          $.ajax(baseUrl+"/view/"+tempPacient+"/body_temperature",{
            type: "GET",
            headers: {
              "Authorization": getAuthorization()
            },
            success: function(data){
              $("#temperature").html(data[0].temperature+data[0].unit);
            },
            error: function(){
              alert("prišlo je do napake");
            }
          });
      }
  });
}
//funkcija za prikaz grafa
function renderGraph(elem){
  $(".d3-tip").hide();
  $("#tipPodatkov").html("  -  "+elem.innerHTML+" ");
  var info = elem.getAttribute("getid");
  var dataId = elem.getAttribute("dataId");
  var ehrId = $("#input-ehrId").val();
  $.ajax(baseUrl+"/view/"+ehrId+"/"+info,{
    type:"GET",
    headers: {
      "Authorization": getAuthorization()
    },
    success:function(data){
      $("#tipPodatkov").append("("+data[0].unit+")");
      //SCALES AND POSTIIONING
      var margin = {left:30, top:10, right:10, bottom: 50};
      var height = 500;
      var scale = d3.scaleLinear()
        .domain([Math.max.apply(null, data.map((e)=>e[dataId]))+40, 0])
        .range([0,height-margin.bottom]);

      //SVG
      var graph = d3.select("#data").selectAll("rect").data(data);
      var x_axis = d3.axisLeft().scale(scale);
      var col_w = 40;

      //TIPS
      var tip = d3.tip().attr("class", "d3-tip").html((e)=>{
        return "<p>"+(new Date(e.time)).toLocaleString()+"</p><p>"
        +elem.innerHTML[0].toUpperCase()+elem.innerHTML.substring(1,elem.innerHTML.length)
        +": "+e[dataId]+e.unit+"</p>";
      });
      d3.select("#data").call(tip);
      
      //ENTER
      var enter = graph.enter()
      .append("rect")
      .attr("width", col_w)
      .attr("fill", "teal")
      .attr("class", "bars");
      
      //UPDATE
      graph.merge(enter)
      .attr("height", (e)=>(height-margin.bottom)-scale(e[dataId]))
      .attr("x", (e,i)=>i*(col_w+5)+40)
      .attr("y", (e)=> margin.top + scale(e[dataId]))
      .on("mouseover", tip.show)
      .on("mouseout", tip.hide);
      
      //ADDING AXIS
      d3.select("svg > g").remove()
      d3.select("#data").append("g").attr("transform", `translate(${margin.left},${margin.top})`).call(x_axis);
      
      graph.exit().remove();
    },
    error:function(error){
      alert("Opala, prišlo je do napake");
      console.log(error);
    }
    
  });
}
//funkcija za posodobitev dropdownmenija, ki jo kliče funkcija generiranje()
function updateDropdown(){
  for(var elem of $("#dropdownUsers")[0].children){
    $(elem).off();
    $(elem).on("click", function(){
      $("#input-ehrId").val(pacienti[this.children[0].getAttribute("name")]);
      $("#input-div").addClass("has-success");
    });
  }
}
//funkcija ki se poklice ob kliku na gumb "Diagnoza"
function diagnoza(){
  if(selectedSymptoms.length < 3){
    selectedSymptoms=[];
    $("#notedSymptoms").html("");
    alert("Vnesite vsaj tri simptome");
    return;
  }
  var height = parseFloat($("#height").attr("data"))/100;
  var weight = parseFloat($("#weight").attr("data"));
  var bmi = weight / (height*height);
  if(bmi>30||bmi<19){
    selectedSymptoms.push({id:bmi>30?"p_7":"p_6", choice_id:"present", initial:"true"});
  }
  var appID = "1e0d098a";
  var appKEY = "e9bbf47fc0f13bd68b54443d752ab1be";
  $.ajax("https://api.infermedica.com/v2/diagnosis", {
    type: "POST",
    contentType: "application/json",
    headers: {
      "App-Id": appID,
      "App-Key": appKEY
    },
    data: JSON.stringify({
      sex: $("#gender").attr("gender"),
      age: parseInt($("#age").html()),
      evidence: selectedSymptoms
    }),
    success: function(data){
      console.log(data);
      for(var elem of data.conditions){
        $.ajax("https://en.wikipedia.org/w/api.php?action=query&format=json&list=search&utf8=1&srsearch="+elem.name.replace(" ", "%20")+"&origin=*", {
          type: "GET",
          prob: elem.probability,
          success:function(wikiData){
            var result = wikiData.query.search[0];
            $("#diagnosisResults").append(
              `<div class='results'><a href='http://en.wikipedia.org/?curid=${result.pageid}'>${result.title}</a> \
              <p style="font-size: 11px">${result.snippet}</p>\
              <p>${Math.round(this.prob*10000)/100} %</p> \
              </div>`
              );
          }
        });
      }
    }
  });
  selectedSymptoms=[];
  $("#notedSymptoms").html("");
}

$(window).load(function(){
    updateDropdown();
    $.ui.autocomplete.prototype._resizeMenu = function () {
      this.menu.element.outerWidth(this.element.outerWidth());
    }
    $.getJSON("knjiznice/json/symptomData.json", function(data){
      $("#symptomSearch").autocomplete({
        minLength: 2,
        source:function(req, res){
          var rezultati = $.ui.autocomplete.filter(data.map(function(e){
            return {label:e.common_name, value:e.common_name, id:e.id}
          }), req.term);
          res(rezultati.slice(0,10));
        },
        select:function(event, ui){
          selectedSymptoms.push({id:ui.item.id, choice_id:"present", initial:"true"});
          $("#notedSymptoms").append("<span class='label label-info'>"+ui.item.label+"</span>");
          $(this).val("");
          event.preventDefault();
        }
      });
      $("#symptomSearch").attr("disabled", true);
    });
    
    $("#manButton").on("click",function(){
      spol="MALE";
    });
    $("#womanButton").on("click", function(){
      spol="FEMALE";
    });
    $("#input-ehrId").on("input", function(){
      if($(this).val().length != 36){
        $("#input-div").removeClass("has-success");
      };
    });
    $("a[getId]").on("click", function(){
      renderGraph(this);
    });
    $("#changeAvatar").on("click", changeAvatar);
    $("#dodaj").on("click", dodaj);
    $("#prikaz").on("click", prikaz);
    $("#diagnoza").on("click", diagnoza);
    $("#generiranjePodatkov").on("click", generirajPodatke);
});